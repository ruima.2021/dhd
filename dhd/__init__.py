# -*- coding: utf-8 -*-

"""Top-level package for DistrictHeatingDesign."""

__author__ = """Pablo Puerto"""
__email__ = "info@crem.ch"
__version__ = '0.1.3'
