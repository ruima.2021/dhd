========
dhd.plot
========

Module to represent graphically the different geometric objects (streets,
buildings,...) of the city.

Once instantiated a background graphical interface with the city buildings and
the street network is displayed which can be used to plot all the other
geometric objects encountered.

Class
-----

.. automodule:: dhd.plot
	:members:
	:private-members:
