==============
dhd.exceptions
==============

List of the custom exceptions used in the different modules of the package *dhd*.

Exceptions
----------

.. automodule:: dhd.exceptions
	:members:
	:private-members: