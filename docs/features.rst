============
dhd.features
============

Module to get quantitative information on the computed district heating
network and to represent it on the plot interface created with the module
*dhd.plot*.

Inputs
------

All the information on the final system design is stored in the dataframes
*terminals* and *pipelines* returned by the module *dhd.load*. These dataframes
together with the plot interface created with the module *dhd.plot* are
the necessary inputs of this module.

Functions
---------

.. automodule:: dhd.features
	:members:
	:private-members:
